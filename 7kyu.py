# Kata "User class for Banking System"
#https://www.codewars.com/kata/5a03af9606d5b65ff7000009/train/python

#solution:

class User(object):
    def __init__(self, name, balance, checking_account):
        self.name = name
        self.balance = balance
        self.checking_account = checking_account
    
    def withdraw(self, draw_money):
        if self.balance - draw_money <= 0:
            raise ValueError()
        else:
            self.balance -= draw_money
            return f'{self.name} has {str(self.balance)}.'
        
    def check(self, user, money):
        if not user.checking_account:
            raise ValueError(f'{user.name} can\'t withdraw {str(user.money)}, he only has {str(user.balance)}')
        if user.balance <= money:
            raise ValueError(f'{user.name} doesn\'t have enough money')
        self.balance += money
        user.balance -= money
        return f'{self.name} has {str(self.balance)} and {user.name} has {str(user.balance)}.'
    
    def add_cash(self, money):
        self.balance += money
        return f'{self.name} has {str(self.balance)}.'

# Kata "Method For Counting Total Occurence Of Specific Digits"
#https://www.codewars.com/kata/56311e4fdd811616810000ce/train/python

#solution:

class List(object):
    def count_spec_digits(self, integers_list, digits_list):
        result = []
        
        for element in range(len(digits_list)):
            digits_list[element] = str(digits_list[element])
            
        for element in range(len(integers_list)):
            integers_list[element] = str(integers_list[element])
            
        for x in digits_list:
            count = 0
            for y in integers_list:
                count += y.count(x)
            result.append((int(x), count))
        return result

# Kata "What a "Classy" Song"
# https://www.codewars.com/kata/6089c7992df556001253ba7d/train/python

#solution:

class Song(object):
    
    def __init__(self, title, artist):
        self.title = title
        self.artist = artist
        self.listener = []
        
    def how_many(self, listeners):
        counter = []
        for listener in listeners:
            if listener.upper() in self.listener:
                continue
            else:
                self.listener.append(listener.upper())
                counter.append(listener)
        return len(counter)