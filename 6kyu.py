# Kata "Simple Substitution Cipher Helper"
#https://www.codewars.com/kata/52eb114b2d55f0e69800078d/train/python

#solution 1:

import string

class Cipher(object):
    def __init__(self, map1, map2):
        
        self.table = str.maketrans(map1, map2)
        self.reverse_table = str.maketrans(map2, map1)
        
    def encode(self, s):
        
        return s.translate(self.table)
    
    def decode(self, s):
        
        return s.translate(self.reverse_table)
    
def encode(map1, map2, s):

    result1 = map(lambda x: map2[map1.index(x)] if x in map1 else x, s)
    result1 = ''.join(result1)

    return result1
#     return "".join(map(lambda x: map2[map1.index(x)] if x in map1 else x, s))

def decode(map1, map2, s):

    result2 = map(lambda x: map1[map2.index(x)] if x in map2 else x, s)
    result2 = ''.join(result2)

    return result2
#     return "".join(map(lambda x: map1[map2.index(x)] if x in map2 else x, s))

#solution 2:

class Cipher(object):
    def __init__(self, map1, map2):
        
        self.table = str.maketrans(map1, map2)
        self.reverse_table = str.maketrans(map2, map1)
        
    def encode(self, s):
        
        return s.translate(self.table)
    
    def decode(self, s):
        
        return s.translate(self.reverse_table)